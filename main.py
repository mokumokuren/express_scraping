#!/usr/bin/python3

from typing import List
from fastapi import FastAPI
from sqlalchemy import and_
from sqlalchemy.orm import sessionmaker, joinedload, with_loader_criteria, Session, Query

from database import create_connection, models, schemas
from datetime import datetime

app = FastAPI()
connection = create_connection()
sm = sessionmaker(bind=connection, expire_on_commit=False)

def get_up_down(dt) -> (datetime, datetime):
    now = datetime.now()
    midnight = now.replace(
        hour=23,
        minute=59,
        second=59,
        microsecond=0
    )

    return midnight, now

@app.get('/movies', response_model=List[schemas.Movie])
async def movies():
    movies: List[models.Movie]
    session: Session
    with sm.begin() as session:
        movies = session.query(models.Movie).all()

    return movies

@app.get('/movies/today', response_model=List[schemas.Movie])
async def today():
    up, down = get_up_down(datetime.now())

    movies: List[models.Movie]
    session: Session
    with sm.begin() as session:
        movies = session\
            .query(models.Movie)\
            .options(
                joinedload(models.Movie.sessions, innerjoin=True),
                with_loader_criteria(
                    models.Movie.sessions,
                    and_(
                        models.Session.planed_at >= down,
                        models.Session.planed_at < up
                    )
                )
            )\
            .all()

    return movies

@app.get('/movies/{mid}')
async def movie(mid: str, response_model=schemas.Movie):
    movie: models.Movie
    session: Session
    with sm.begin() as session:
        movie = session.query(models.Movie).filter_by(mid=mid).first()
    
    return movie

@app.on_event('shutdown')
def close_connection():
    connection.dispose()
    print('Connection has been disposed of')