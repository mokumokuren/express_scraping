# Type hints import
from datetime import datetime
# Application related import
from sqlalchemy import Column, DateTime, Integer, String, ForeignKey

from . import constants

class Session(constants.Base):
    # ORM mapping
    __tablename__ = 'sessions'

    # Session ID
    sid: int = Column(Integer, primary_key=True, autoincrement=True)
    # Theater ID
    tid = None # TODO: implement Theater Model and table
    # Movie ID
    mid: str = Column(String, ForeignKey('movies.mid'))
    planed_at: datetime = Column(DateTime)