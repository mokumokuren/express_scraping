# Type hints import
from __future__ import annotations
from typing import List
from datetime import datetime
# Application related imports
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
# Local import
from .session import Session
from . import constants

class Movie(constants.Base):
    # ORM mapping
    __tablename__ = 'movies'
    mid = Column(String(12), primary_key=True) 
    title = Column(String(255))
    poster = Column(String(255))
    trailer = Column(String(255))
    synopsis = Column(String)
    sessions = relationship('Session', lazy='selectin')

    def __init__(self, title: str, poster: str, trailer: str, synopsis: str):
        self.mid = constants.unique_id()
        self.title = title
        self.poster = poster
        self.trailer = trailer
        self.synopsis = synopsis
        self.sessions: List[Session] = []

    def __eq__(self, other: Movie) -> bool:
        return isinstance(other, Movie) and other.title == self.title 

    def add_sessions(self, date: datetime, sessions: List[str]) -> None:
        for session in sessions:
            hour, minute = session.split(':')
            date = date.replace(
                hour=int(hour),
                minute=int(minute),
                second=0,
                microsecond=0
            )

            self.sessions.append(Session(planed_at=date))