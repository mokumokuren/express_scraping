# Type hints import
from __future__ import annotations
from typing import List
from .movie import Movie

class MovieList:
    def __init__(self):
        self.movies: List[Movie] = []

    def __iter__(self) -> MovieList:
        self.index: int = 0
        return self
    
    def __next__(self) -> Movie:
        if self.index == len(self.movies):
            raise StopIteration()
        
        movie = self.movies[self.index]
        self.index += 1

        return movie

    # normally i will not use multiple index access
    def __getitem__(self, index) -> Movie:
        return self.movies[index]

    # return the movie's index inside the self.movies list
    # if it is already there, else add it and return its index
    def get_or_add(self, movie) -> int:
        if movie in self.movies:
            return self.movies.index(movie)

        self.movies.append(movie)
        return len(self.movies) - 1

    def add_session_to_movie(self, date, sessions, movie) -> None:
        movie_index = self.get_or_add(movie)
        self.movies[movie_index].add_sessions(date, sessions)