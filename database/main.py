'''
A module responsible of connecting us to the database,
cleaning all the tables, and filling them with our
scraped data.
'''
# Type hints import
from typing import List
from sqlalchemy.engine import Engine

# Application related import
from os import getenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .models import Base, Movie, Session

# Delete and create the needed tables
def blank_new_tables(engine: Engine) -> None:
    tables = [Movie.__table__, Session.__table__]

    # Session table should be deleted first because of the
    # foreign key dependency
    Base.metadata.drop_all(engine, tables.reverse())
    # But Movies table should be created first
    Base.metadata.create_all(engine, tables)

def create_connection() -> Engine:
    'Creates a new SQL engine using environment variable `DATABASE_URL`.'
    url = getenv('DATABASE_URL')
    if not url or url == '' or not url.startswith('postgres'):
        raise Exception('DATABASE_URL is missing or invalid.')

    if url.startswith('postgres://'):
        url = url.replace('postgres://', 'postgresql://')

    return create_engine(url)

def insert_data_into(engine: Engine, movies: List[Movie]) -> None:
    sm = sessionmaker(bind=engine)

    with sm.begin() as session:
        session.add_all(movies) 

def update_database_with(engine: Engine, data: List[Movie]) -> None:
    'Cleans the db\'s tables and inserts [data].'
    blank_new_tables(engine)

    insert_data_into(engine, data)