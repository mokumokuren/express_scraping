from typing import List, Dict, Any
from pydantic import BaseModel
from pydantic.utils import GetterDict
from collections import defaultdict
from datetime import date, time

from .. import models 

class MovieGetter(GetterDict):
    def get(self, key: str, default: Any) -> Any:
        self._obj: models.Movie
        attr = getattr(self._obj, key)

        if not key == 'sessions':
            return attr
        
        # if we get there, attr is a List[models.Session]
        sessions: List[models.Session] = attr
        # We gonna change the JSON representation of 
        # [models.Movie.sessions] into an subobject containing
        # a date as its keys and a list of every time there is 
        # a session available.
        sess_repr = defaultdict(list)
        for session in sessions:
            date, time = session.planed_at.date(), session.planed_at.time()
            sess_repr[date].append(time)
        
        return sess_repr


class Movie(BaseModel):
    mid: str
    title: str
    poster: str
    trailer: str
    synopsis: str
    sessions: Dict[date, List[time]] 

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        getter_dict = MovieGetter