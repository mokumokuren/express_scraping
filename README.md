# Mokumokuren back-end
## Description 
A fastapi python webserver. Using requests for scraping and sqlalchemy for database communication.

## Installation
It should be as easy as just launching the following commands.
```sh
# Cloning repo over http
git clone https://gitlab.com/mokumokuren/fastapi_backend
# Installing requirements
cd fastapi_backend
pip install -r requirements.txt
# Launching webserver
gunicorn -w 3 -k uvicorn.workers.UvicornWorker index:app
```

Alternatively, you can use a virtualenv before requirements installation
```sh
python -m venv virtualenv
source virtualenv/bin/activate
```