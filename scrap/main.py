'''
The module responsible of scraping the UGC website infos
and returning them as a MovieList object.
'''
# Type hints import
from typing import List
from database import Movie

from .theater import TheaterFactory, TheaterType

def scrap_info_from_theaters() -> List[Movie]:
    '''
    Go through every theater, retrieving their data
    by scraping. Return a list containing the data as 
    multiple Movie object.
    '''
    data = []
    for each in TheaterType:
        theater = TheaterFactory(each)
        theater_data = theater.movies()
        data += theater_data
    
    return data