from enum import Enum

from .kind import *

class TheaterType(Enum):
    UGC = Ugc
    UTOPIA = Utopia

def TheaterFactory(t: TheaterType) -> Theater:
        return t.value()