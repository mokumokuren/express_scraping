# Type hints import 
from typing import List, Tuple
from bs4.element import Tag
# Application import
from bs4 import BeautifulSoup
from re import compile as regex
from requests import get
import datetime
import dateparser
# Local import
from .theater import Theater
from database import MovieList, Movie

# Custom type for hinting
MovieData = Tuple[Movie, List[str]]

def only_available(movie_html: Tag) -> bool:
    '''
    Used to check if the movie is available to watch in the next week and if it really is a movie and not a piece nor an opera.
    '''
    # only movie's div contains a html tag which id modal-see-video-title
    is_a_movie = movie_html.select_one('#modal-see-video-title') != None
    # Check if there is available sessions
    is_there_a_session = movie_html.find(string=regex("Prochaine")) == None

    return is_a_movie and is_there_a_session

class Ugc(Theater):
    # url w/o the date parameter
    BASE_URL = "https://www.ugc.fr/showingsCinemaAjaxAction!getShowingsForCinemaPage.action?cinemaId=1"

    def __init__(self):
        self._current = datetime.datetime.today()
        self._week = self._current + datetime.timedelta(weeks=1)

        self._movie_list = MovieList()
        
    def _add_day(self) -> None:
        'Adds a day to [self._current].'
        self._current += datetime.timedelta(days=1)

    def _keep_retrieving(self) -> bool:
        '''
        Checks that [self._current] hasn't exceeded [self._week].
        If so we should keep looping and retrieving data.
        '''
        return self._current <= self._week

    def _get_and_dissect_infos(self) -> None:
        '''
        Makes a GET HTTP request to retrieve projected movies for [self.current]
        Creates then a BS object based on the response and stores it in [self._soup] 
        '''
        uri = f'{Ugc.BASE_URL}&date={self._current.strftime("%d%%2F%m%%2F%y")}'

        response = get(uri)
        self._soup = BeautifulSoup(response.text, 'html.parser')

    def _retrieve_movie_data(self) -> List[MovieData]:
        '''
        Go through every movie tag the page contains and extracts data from each.
        Returns the array of all the data combined.
        ''' 
        data = []
        ugc_sliders = self._soup.find_all(only_available, recursive=False)

        for slider in ugc_sliders:
            result = self._movie_and_session_from(slider)
            data.append(result)
        
        return data

    def _movie_and_session_from(self, movie_html: Tag) -> MovieData:
        '''
        Retrieve movies infos from [movie_html] tag object. 
        Returns a tuple following:
            0 -> a Movie object created from the data
            1 -> a List[str] that represents all movie's sessions at [self._current]
        '''
        # title
        title = movie_html.find(id='modal-see-video-title').text.strip()
        # poster
        poster = movie_html.select_one('a img')['data-src']
        # trailer
        trailer = movie_html.select_one('div.content video source')['data-src']
        # sessions
        sessions = self._get_next_sessions(movie_html)
        # ugc movie id
        synopsis = self._get_synopsis(movie_html)

        return (Movie(title, poster, trailer, synopsis), sessions)

    def _get_next_sessions(self, movie_html: Tag) -> List[str]:
        'Strips from [movie_html] the available movie\'s sessions'
        return [session.text.strip() for session in movie_html.select('.screening-start')]

    def _get_synopsis(self, movie_html: Tag) -> str:
        '''
        Extracts the unique movie's link, then emit a GET request.
        We scrap the synopsis from that new BS object.
        '''
        link = movie_html.select_one('div.block--title a')['href']

        response = get(f'https://ugc.fr/{link}')
        soup = BeautifulSoup(response.text, 'html.parser')

        return soup.select_one('.group-title ~ p').text

    def movies(self) -> List[Movie]:
        while self._keep_retrieving():
            self._get_and_dissect_infos()

            data = self._retrieve_movie_data()
            for (movie, sessions) in data:
                self._movie_list.add_session_to_movie(
                    self._current,
                    sessions,
                    movie
                )

            self._add_day()

        return self._movie_list.movies