# Type hints
from typing import List

from .theater import Theater
from database import MovieList, Movie

class Utopia():
   def movies(self) -> List[Movie]:
       return [Movie('Utopia Test', 'none', 'none', 'a placeholder just so there is an utopia record in the db')]