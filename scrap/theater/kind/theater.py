# Type hints import
from typing import List
from database import Movie

from abc import ABC, abstractmethod

class Theater(ABC):
    @abstractmethod
    def movies(self) -> List[Movie]:
        '''
        A method that return all the scraped theater's data
        as a list of Movie object that can be inserted
        '''