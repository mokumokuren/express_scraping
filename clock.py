#!/usr/bin/python3

from apscheduler.schedulers.blocking import BlockingScheduler
from os import getenv
from pytz import timezone

from scrap import scrap_info_from_theaters 
from database import create_connection, update_database_with

def daily_job():
    print('Cronjob is starting')

    engine = create_connection()

    data = scrap_info_from_theaters()

    update_database_with(engine, data)

    engine.dispose()

    print('Cronjob has terminated')

if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(
        daily_job, 
        'cron', 
        hour=23, 
        minute=30, 
        timezone=timezone('Europe/Paris')
    )
    scheduler.start()